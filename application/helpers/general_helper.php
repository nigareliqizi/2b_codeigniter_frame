<?php

function calculateSum($a, $b){
	return $a + $b;
}

function getSesUserName(){
	return $_SESSION['name'] ?? 'bele bir sessiya yoxdur';
}


/*

1 - AZN
2 - USD
3 - EUR
4 - RUB
*/

function currencyMap($currencyId){
	$array = [
		'1' => 'AZN',
		'2' => 'USD',
		'3' => 'EUR',
		'4' => 'RUB'
	];

	return $array[$currencyId];
}