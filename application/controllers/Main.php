<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('products_model', 'products');
		$this->load->helper('general_helper');
	}


	public function index(){

		/*
		Modeli yuklemek ucun $this->load->model('model_name');
		Modele muraciet etmek ucun
		$this->model_name->metod_name();
		Butun datalari html-e gondermek ucun onu array formasinda yazmaq lazimdir: Meselen:

		$data['deyishenin_adi'] = "sizin datalariniz olacaq"
		*/
		// $view['sum'] = calculateSum(2,4);
		$view['products'] = $this->products->get();
		$view['headerText'] = "HeaderText example";
		$view['body'] = 'index.php';
		$this->load->view('layout', $view);	
	}

	public function details($id){
		$view['productDetails'] = $this->products->getById($id);
		// echo $this->db->last_query();
		// print_r($view['productDetails']); exit;
		$view['body'] = 'details.php';

		$this->load->view('layout', $view);
	}

	public function women(){
		$data['body'] = 'women.php';
		$this->load->view('layout', $data);
	}
}