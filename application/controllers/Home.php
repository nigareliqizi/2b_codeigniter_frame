<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

public function index(){
	$data['menus'] = array(

		'home' => 'Home', 
		'about'  =>'About', 
		'contact'  => 'Contact Us');

	$this->load->view('example', $data);
}

}