<?php

class Products_model extends CI_Model{

	public function get(){
		$query = "SELECT * FROM products order by id DESC";

		$result = $this->db->query($query);

		return $result->result_array();
	}


	public function get2(){
		$this->db->select('*');
		$this->db->from('products');
		return $this->db->query()->result_array();
	}

	public function getById($id){
		$this->db->select('products.*, products.name as product_name, categories.*, categories.name as category_name');
		$this->db->from('products');
		$this->db->join('categories', 'products.category_id = categories.id');
		$this->db->where('products.id', $id);
		return $this->db->get()->result_array()[0] ?? []; 
		// return $this->db->query()->result_array();
	}

	public function insert($data){
		
		$this->db->insert('products', $data);
	}

	public function update($data, $id){
		$this->db->update('products', $data);
		$this->db->where('id', $id);
	}
}